import { createApp } from "vue";
import App from "./App.vue";

import "./plugins/tailwind.css";
import "bootstrap-icons/font/bootstrap-icons.css";

createApp(App).mount("#app");
